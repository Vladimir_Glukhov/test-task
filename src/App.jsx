import React from 'react';
import history from './router/history';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import LoginPage from './components/pages/LoginPage';
import MainLayout from './components/pages/MainLayout';

function App() {
  return (
    <Router history={history}>
      <MainLayout>
        <Switch>
          <Route exact path="/">
          </Route>
          <Route exact path="/mainpage">
            главная страница
          </Route>
          <Route path="/login">
            <LoginPage type={'LOGIN'} />
          </Route>
          <Route path="/registration">
            <LoginPage type={'REGISTRATION'} />
          </Route>
        </Switch>
      </MainLayout>
    </Router>
  );
}

export default App;
