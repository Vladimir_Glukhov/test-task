import React from 'react';

export default ({ id, name, label, placeholder, isError, type = 'text', value, onChange }) => (
    <div className="label-group">
        <label htmlFor={id}>{label}</label>
        <input className={isError ? 'input-error' : ''} name={name} id={id} type={type} placeholder={placeholder} value={value} onChange={onChange}/>
    </div>
);