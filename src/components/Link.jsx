import React from 'react';
import { Link } from 'react-router-dom';

export default ({ to, title }) => <div className="page-link"><Link to={to} >{title}</Link></div>