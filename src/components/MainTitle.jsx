import React from 'react';

export default () => (
    <h1 className="main-title">
        <span className="main-title--bold margin-r-0">Web</span>
        <span className="main-title--thin">App</span>
    </h1>);