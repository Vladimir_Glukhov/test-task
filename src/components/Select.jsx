import React, { useState } from 'react';

export default ({ options, value, onChange, name, id }) => {
    const [status, setStatus] = useState(false);
    const onFocus = () => setStatus(true);
    const onClick = (s) => {
        setStatus(false);
        onChange && onChange(s);
    };
    const handleChange = e => onChange && onChange(e.target.value.trim());
    const search = options.filter(c => c.toLowerCase().indexOf(value.toLowerCase()) !== -1);

    return (
        <div>
            <input autoComplete="off" type="text" name={name} id={id} value={value} onChange={handleChange} onFocus={onFocus} />
            {status &&
            <div className="select-container">
                <div className="select-container-values">
                    {search.map((s, i) => <p className="select-value" key={i} onClick={(e) => {
                        onClick(s);
                    }}
                    >{s}</p>)}
                </div>
            </div>}
        </div>
    )
}