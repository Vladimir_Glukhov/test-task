import React, { useState } from 'react';
import LabelInput from '../LabelInput';
import Link from '../Link';

import { formIds } from './constants';

const LoginForm = ({ onSubmit, errors = {}}) => {
    const [login, setLogin] = useState('');
    const [password, setPassword] = useState('');
    const onLoginChange = e => {
        const value = e.target.value.trim();
        setLogin(value);
    };
    const onPasswordChange = e => {
        const value = e.target.value;
        setPassword(value);        
    };
   
    return (<>
        <h2 className="page-title">Войдите в свой профиль</h2>
        <form className="login-form" onSubmit={onSubmit}>
            <LabelInput id={formIds.login} name={formIds.login} isError={errors[formIds.login]} label="Логин" placeholder="Введите логин" value={login} onChange={onLoginChange} />
            <LabelInput id={formIds.password} type="password" isError={errors[formIds.password]} name={formIds.password} label="Пароль" placeholder="Введите пароль" value={password} onChange={onPasswordChange}/>
            <input className="form-button" type="submit" value="Войти" />
        </form>
        <div className="link-container">
            <Link to='/' title="Я забыл пароль" />
            <Link to='/registration' title="Регистрация" />
        </div>
    </>
    );
};

export default LoginForm;