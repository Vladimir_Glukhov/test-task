import React, { useEffect, useState } from 'react';
import LabelInput from '../LabelInput';
import Link from '../Link';
import Select from '../Select';

import { formIds, countryOptions, cityOptions } from './constants';

const useFormInput = (def = '') => {
    const [value, setValue] = useState(def);
    const onChange = (e) => setValue(e.target.value.trim());

    return [value, onChange];
}

const RegistrationForm = ({ onSubmit, errors }) => {
    const [country, setCountry] = useState(countryOptions[0]);
    const [city, setCity] = useState(cityOptions[countryOptions[0]][0]);
    const [name, setName] = useFormInput();
    const [system, setSystem] = useFormInput();
    const [password, setPassword] = useFormInput();
    const [confPassword, setConfPassword] = useFormInput();
    const [secondName, setSecondName] = useFormInput();
    const [lastName, setLastName] = useFormInput();
    const [email, setEmail] = useFormInput();

    const [correctCityOptions, setOptions] = useState(cityOptions[country]);

    useEffect(() => {
        if (countryOptions.includes(country)) {
            setOptions(cityOptions[country]);
            setCity(cityOptions[country][0]);
        }
    }, [country]);

    return (<>
        <h2 className="page-title">Создание нового аккаунта</h2>
        <form className="login-form" onSubmit={onSubmit}>
            <LabelInput isError={errors[formIds.secondName]} id={formIds.secondName} name={formIds.secondName} label="Фамилия" placeholder="Введите Вашу фамилию" value={secondName} onChange={setSecondName} />
            <LabelInput isError={errors[formIds.name]} id={formIds.name} name={formIds.name} label="Имя" placeholder="Введите Ваше имя" value={name} onChange={setName} />
            <LabelInput isError={errors[formIds.lastName]} id={formIds.lastName} name={formIds.lastName} label="Отчество" placeholder="Введите Ваше отчество" value={lastName} onChange={setLastName} />
            <LabelInput isError={errors[formIds.email]} id={formIds.email} type="email" name={formIds.email} label="E-MAIL" placeholder="Укажите e-mail" value={email} onChange={setEmail} />
            <div className="label-group">
                <label className="">Укажите город и страну</label>
                <Select id={formIds.country} name={formIds.country} options={countryOptions} value={country} onChange={setCountry} title="Укажите страну" />
                <Select id={formIds.city} name={formIds.city} options={correctCityOptions} value={city} onChange={setCity} title="Укажите страну" />
            </div>
            <LabelInput isError={errors[formIds.system]} id={formIds.system} name={formIds.system} label="Укажите ОС телефона" placeholder="" value={system} onChange={setSystem} />
            <LabelInput isError={errors[formIds.password]} id={formIds.password} type="password" name={formIds.password} label="Пароль" placeholder="Введите пароль" value={password} onChange={setPassword} />
            <LabelInput isError={errors[formIds.confirmPassword]} id={formIds.confirmPassword} type="password" name={formIds.confirmPassword} label="Подтверждение пароля" placeholder="Повторите пароль" value={confPassword} onChange={setConfPassword} />
            <input className="form-button" type="submit" value="Создать" />
        </form>
        <div className="link-container">
            <Link to='/login' title="Назад" />
        </div>
    </>);
};

export default RegistrationForm;