export const formIds = {
    login: 'login',
    password: 'password',
    confirmPassword: 'confirmPassword',
    name: 'name',
    secondName: 'secondName',
    lastName: 'lastName',
    country: 'country',
    city: 'city',
    email: 'email',
    system: 'system'
};

export const countryOptions = ['США', 'Россия', 'Япония'];
export const cityOptions = {
    [countryOptions[0]]: ['Нью-Йорк', 'Калифорния'],
    [countryOptions[1]]: ['Москва', 'Саратов', 'Санкт-Петербург', 'Нижний Новгород', 'Тверь'],
    [countryOptions[2]]: ['Токио', 'Киото']
}