import React, { useState } from 'react';
import MainTitle from '../MainTitle';

import LoginForm from '../forms/LoginForm';
import RegistrationForm from '../forms/RegistrationFrom';

import SMSValidator from './SMSValidator';

import { loginUser } from '../../utils/utils';
import { validateFormElements, validateRegistrationElements } from '../../utils/validation';

export default ({ type }) => {
    const [errors, setErrors] = useState({});
    const [showSMSvalidator, setStatus] = useState(false);

    const onLoginSubmit = (e) => {
        e.preventDefault();
        const elements = e.target.elements;
        const err = validateFormElements(elements);
        setErrors(err);
        const isValid = Object.values(err).every(v => !v);
        if (isValid) {
            const params = { login: elements.login.value, password: elements.password.value };//create cached pass
            loginUser(params).then(result => {
                setStatus(result);
            });
        }
    };
    const onRegistrationSubmit = (e) => {
        e.preventDefault();
        const elements = e.target.elements;
        const err = validateRegistrationElements(elements);
        setErrors(err);
        const isValid = Object.values(err).every(v => !v);
    };

    return (<>
        <div className="form-container">
            <MainTitle />
            {type === 'LOGIN' ? <LoginForm onSubmit={onLoginSubmit} errors={errors} /> : <RegistrationForm onSubmit={onRegistrationSubmit} errors={errors} />}
            <a href="mailto:support@support.com" className="email-link">Написать в поддержку</a>
        </div>
        {showSMSvalidator && <SMSValidator onCloseSMS={()=>setStatus(false)}/>}
    </>);
};