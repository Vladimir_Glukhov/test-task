import React from 'react';

export default (props) => {
    return (
        <>
            <div className="page-container">
                {props.children}
            </div>
        </>
    );
}