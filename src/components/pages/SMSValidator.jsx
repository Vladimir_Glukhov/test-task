import React, { useState } from 'react';
import { sendSMS } from '../../utils/utils';

import { useHistory } from "react-router-dom";

const SMSValidator = ({ onCloseSMS }) => {
    const [sms, setSMS] = useState('');
    const [isRequested, setStatus] = useState();
    const history = useHistory();

    const onSubmitSMSClick = () => {
        setStatus(true);
        sendSMS(sms).then((res) => {
            console.log(res);
            setStatus(false);
            onCloseSMS && onCloseSMS();
            if (res) {
                history.push('/mainpage');
            }
        });
    };

    const onChange = e => setSMS(e.target.value);

    return (
        <>
            <div className="sms-layout" onClick={onCloseSMS} />
            <div className="sms-validator">
                {!isRequested ? <>
                    <label>Введите смс код</label>
                    <input id="sms-validator" tabIndex={0} value={sms} onChange={onChange} autoFocus name="sms-validator" />
                    <button onClick={onSubmitSMSClick}>OK</button>
                </> : <span>Пожалуйста подождите</span>}
            </div>
        </>
    );
}

export default SMSValidator;