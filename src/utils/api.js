const loginUserBE = params => new Promise((resolve, reject) => {
    if (params.login === 'login' && params.password === 'password') resolve({ token: 123 });
    reject({ error: 'invalid' });
});
const sendSMSBE = sms => new Promise((resolve, reject) => {
    setTimeout(()=> resolve(+sms === 1234), 1000);
});

export const loginUserApi = async params => {
    try {
        const response = await loginUserBE(params);
        return true;
    } catch (e) {
        return false;
    }
};

export const sendSMSApi = async sms => {
    try {
        const result = await sendSMSBE(sms);
        return result;
    } catch (e) {
        return false;
    }
};