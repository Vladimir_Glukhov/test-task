import { loginUserApi, sendSMSApi } from './api.js';

export const loginUser = params => loginUserApi(params);
export const sendSMS = sms => sendSMSApi(sms);