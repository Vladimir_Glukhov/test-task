import { formIds } from '../components/forms/constants';

export const validateFormElements = (elements) => {
    const errors = {};
    const password = elements[formIds.password].value;
    const login = elements[formIds.login].value;

    if (!login || login.length < 5) errors[formIds.login] = true;
    if (!password || password.length < 5) errors[formIds.password] = true;

    return errors;
}

export const validateRegistrationElements = elements => {
    const errors = {};
    const password = elements[formIds.password].value;
    const confirmPassword = elements[formIds.confirmPassword].value;
    const name = elements[formIds.name].value;
    const secondName = elements[formIds.secondName].value;
    const lastName = elements[formIds.lastName].value;
    const email = elements[formIds.email].value;

    if (!name || name.length < 5) errors[formIds.name] = true;
    if (!secondName || secondName.length < 5) errors[formIds.secondName] = true;
    if (!lastName || lastName.length < 5) errors[formIds.lastName] = true;
    if (!email || email.length < 5) errors[formIds.email] = true;
    if (!password || password.length < 5 || confirmPassword !== password) errors[formIds.password] = true;
    if (!confirmPassword || confirmPassword.length < 5 || confirmPassword !== password) errors[formIds.confirmPassword] = true;

    return errors;
}